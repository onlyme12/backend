package main

//Notice: typing problems: //https://newfivefour.com/golang-interface-type-assertions-switch.html
//append problems: https://play.golang.org/p/CjR88q7CIo
import (
	"./api"
	_ "./docs"
	"./model"
	"./util"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
	"github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	"time"
)
var cv model.CV

// @title Swagger Example API
// @version 1.0
// @description This is a cvGeo server.
// @termsOfService http://swagger.io/terms/

// @contact.name API Support
// @contact.email ffischer1984@googlemail.com

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host localhost:8080
// @BasePath /
func main() {
	gin.SetMode(gin.ReleaseMode)

	r := gin.Default()
	r.Use(static.Serve("/assets", static.LocalFile("./public", false)))
	r.Use(static.Serve("/client", static.LocalFile("./public/client", false)))
	r.StaticFile("/", "./public/*")
	r.GET("/cv", preflight, api.GET_CV)
	r.GET("/cv/filter", preflight, api.GET_Filter)
	r.GET("/cv/categories", preflight, api.GET_Categories)
	r.POST("/cv/entry", preflight, api.POST_CVEntry)
	r.DELETE("/cv/entry", preflight, api.DELETE_CVEntry)

	r.GET("/swagger2/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	r.Use(cors.New(cors.Config{

		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"*"},
		AllowHeaders:     []string{"Origin"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))
	r.Run() // listen and serve on 0.0.0.0:8080

}

func preflight(c *gin.Context) {
	c.Header("Access-Control-Allow-Origin", "*")
	c.Header("Access-Control-Allow-Headers", "access-control-allow-origin, access-control-allow-headers")

	api.CV = util.LoadData("./data/ffischer/cv.json")

}





