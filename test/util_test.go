package test

import (
	"../model"
	"../util"
	"fmt"
	"github.com/araddon/dateparse"
	"github.com/thoas/go-funk"
	"testing"
)

func TestLoadData(t *testing.T) {

	var a = util.LoadData("./ErrorData.json")
	fmt.Print(a)
	if len(a.Data) != 0 {
		t.Error("sollte null ergeben, da nicht die gleiche Datenstruktur!")
	}

}

func TestFilterByCategory(t *testing.T) {

	res := util.FilterByCategory(getTestDataForCategoryFilter(), "job")
	if res == nil || len(res) != 1 || res[0].Name != "job" {
		t.Error("jobs could not be filtered")
	}

	res = util.FilterByCategory(getTestDataForCategoryFilter(), "project")
	if res == nil || len(res) != 2 {
		t.Error("project: could not find all items")
	}

}

func TestGetBBoxinOnePlace(t *testing.T) {
	var data model.BBox
	data, _ = util.GetBBox(getTestDataBBoxOneEntryX())
	if data.Min.X != 25 {
		t.Error("minX is 25")
	}
	if data.Max.X != 100 {
		t.Error("maxX is 100")
	}

	data, _ = util.GetBBox(getTestDataBBoxOneEntryY())
	if data.Min.Y != 25 {
		t.Error("minY is 25")
	}
	if data.Max.Y != 100 {
		t.Error("maxY is 100")
	}

	data, _ = util.GetBBox(getTestDataBBoxManyEntriesY())
	if data.Min.Y != 25 {
		t.Error("minY is 25")
	}
	if data.Max.Y != 100 {
		t.Error("maxY is 100")
	}

}

func TestFilterByTime(t *testing.T) {
	leftOutside := 0
	leftHalfInside := 1
	inside := 2
	rightHalfOutside := 3
	rightOutside := 4
	overall := 5

	elemIsMissingInResult := "-elem is missing in resultSet"
	elemNotAllowedToBeIn := "-elem is not allowed to be in resultSet"

	rangeStart, _ := dateparse.ParseAny("2017/01/01")
	rangeEnd, _ := dateparse.ParseAny("2017/12/31")

	resData := util.FilterByTime(getTestDataForTimeFilter(), rangeStart, rangeEnd)

	//check size
	if len(resData) != 4 {
		t.Error("resultSet has not the correct size!")
	}

	/**
	Are the correct elems in the resultSet?
	**/

	//inside the ResultSet
	if !funk.Contains(resData, getTestDataForTimeFilter()[leftHalfInside]) {
		t.Error(getTestDataForTimeFilter()[leftHalfInside].Name, elemIsMissingInResult)
	}
	if !funk.Contains(resData, getTestDataForTimeFilter()[inside]) {
		t.Error(getTestDataForTimeFilter()[inside].Name, elemIsMissingInResult)

	}
	if !funk.Contains(resData, getTestDataForTimeFilter()[rightHalfOutside]) {
		t.Error(getTestDataForTimeFilter()[rightHalfOutside].Name, elemIsMissingInResult)

	}
	if !funk.Contains(resData, getTestDataForTimeFilter()[overall]) {
		t.Error(getTestDataForTimeFilter()[overall].Name, elemIsMissingInResult)

	}
	//should NOT inside!!
	if funk.Contains(resData, getTestDataForTimeFilter()[leftOutside]) {
		t.Error(getTestDataForTimeFilter()[leftOutside].Name, elemNotAllowedToBeIn)

	}
	if funk.Contains(resData, getTestDataForTimeFilter()[rightOutside]) {
		t.Error(getTestDataForTimeFilter()[rightOutside].Name, elemNotAllowedToBeIn)
	}

}

func getTestDataForTimeFilter() []model.CVEntry {
	return []model.CVEntry{
		{
			"leftOutside",
			model.TimeRange{
				"2016/12/21",
				"2016/12/25",
			},
			nil,
			nil,
			"",
			"",
			"",
		},
		{
			"leftHalfInside",
			model.TimeRange{
				"2016/12/21",
				"2017/01/12",
			},
			nil,
			nil,
			"",
			"",
			"",
		},
		{
			"inside",
			model.TimeRange{
				"2017/07/21",
				"2017/12/25",
			},
			nil,
			nil,
			"",
			"",
			"",
		},
		{
			"rightHalfOutside",
			model.TimeRange{
				"2017/12/21",
				"2018/12/25",
			},
			nil,
			nil,
			"",
			"",
			"",
		},
		{
			"rightOutside",
			model.TimeRange{
				"2018/12/21",
				"2018/12/25",
			},
			nil,
			nil,
			"",
			"",
			"",
		},
		{ //insideOutsideLeftAndRight
			"overall",
			model.TimeRange{
				"2016/12/21",
				"2018/12/25",
			},
			nil,
			nil,
			"",
			"",
			"",
		},
	}
}
func getTestDataForCategoryFilter() []model.CVEntry {
	return []model.CVEntry{
		{
			"job",
			model.TimeRange{
				"2016/12/21",
				"2016/12/25",
			},
			nil,
			nil,
			"job",
			"",
			"",
		},
		{
			"edu",
			model.TimeRange{
				"2016/12/21",
				"2017/01/12",
			},
			nil,
			nil,
			"edu",
			"",
			"",
		},
		{
			"publication",
			model.TimeRange{
				"2017/07/21",
				"2017/12/25",
			},
			nil,
			nil,
			"publication",
			"",
			"",
		},
		{
			"project",
			model.TimeRange{
				"2017/12/21",
				"2018/12/25",
			},
			nil,
			nil,
			"project",
			"",
			"",
		},
		{
			"project",
			model.TimeRange{
				"2018/12/21",
				"2018/12/25",
			},
			nil,
			nil,
			"project",
			"",
			"",
		},
		{ //insideOutsideLeftAndRight
			"overall",
			model.TimeRange{
				"2016/12/21",
				"2018/12/25",
			},
			nil,
			nil,
			"",
			"",
			"",
		},
	}
}
func getTestDataBBoxOneEntryX() []model.CVEntry {
	return []model.CVEntry{
		{ //insideOutsideLeftAndRight
			"oneX",
			model.TimeRange{
				"2017/07/21",
				"2017/12/25",
			},
			[]model.Place{
				{
					"middleX",
					model.Location{
						"50",
						"0",
					},
				},
				{
					"lowestX",
					model.Location{
						"25",
						"0",
					},
				},
				{
					"highestX",
					model.Location{
						"100",
						"0",
					},
				},
			},
			nil,
			"",
			"",
			"",
		},
	}
}
func getTestDataBBoxOneEntryY() []model.CVEntry {
	return []model.CVEntry{
		{ //insideOutsideLeftAndRight
			"oneX",
			model.TimeRange{
				"2017/07/21",
				"2017/12/25",
			},
			[]model.Place{
				{
					"middleX",
					model.Location{
						"0",
						"50",
					},
				},
				{
					"lowestX",
					model.Location{
						"0",
						"25",
					},
				},
				{
					"highestX",
					model.Location{
						"0",
						"100",
					},
				},
			},
			nil,
			"",
			"",
			"",
		},
	}
}
func getTestDataBBoxManyEntriesY() []model.CVEntry {
	return []model.CVEntry{
		{ //insideOutsideLeftAndRight
			"oneX",
			model.TimeRange{
				"2017/07/21",
				"2017/12/25",
			},
			[]model.Place{
				{
					"middleX",
					model.Location{
						"0",
						"50",
					},
				},
				{
					"middleX",
					model.Location{
						"0",
						"50",
					},
				},
				{
					"middleX",
					model.Location{
						"0",
						"50",
					},
				},
			},
			nil,
			"",
			"",
			"",
		},
		{ //insideOutsideLeftAndRight
			"oneX",
			model.TimeRange{
				"2017/07/21",
				"2017/12/25",
			},
			[]model.Place{
				{
					"highestY",
					model.Location{
						"0",
						"100",
					},
				},
			},
			nil,
			"",
			"",
			"",
		},
		{ //insideOutsideLeftAndRight
			"oneX",
			model.TimeRange{
				"2017/07/21",
				"2017/12/25",
			},
			[]model.Place{
				{
					"lowestX",
					model.Location{
						"0",
						"25",
					},
				},
				{
					"lowestX",
					model.Location{
						"0",
						"25",
					},
				},
				{
					"lowestX",
					model.Location{
						"0",
						"25",
					},
				},
			},
			nil,
			"",
			"",
			"",
		},
	}
}
