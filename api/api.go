package api

import (
	"github.com/gin-gonic/gin"
	"github.com/araddon/dateparse"
	"sort"
	"../util"
	"../model"
)

var Categories = []string{"job", "edu", "publication", "project"}
var CV  model.CV

// @summary shows all Data
// @ID get-Categories
// @Accept  json
// @Produce  json
// @Success 200 {object} model.CV "ok"
// @Failure 400 {string} string "vermutl datei nicht gefunden"
// @Router /cv/categories [get]
func GET_Categories(c *gin.Context) {
	c.JSON(200, Categories)
}



// @summary filter what you want and gives you a Geo-BoundingBox
// @ID cv/filter?start="date"&end="date&category=id"
// @Accept  json
// @Produce  json
// @Param start query string false "2005/01/01"
// @Param end query string false "2018/12/31"
// @Param category query string false "job"
// @Success 200 {object} model.CVEntry "ok"
// @Failure 400 {string} string "vermutl datei nicht gefunden"
// @Router /cv/filter?start="date"&end="date&category=id" [get]
func GET_Filter(c *gin.Context) {
	var filterByCategory bool = true
	var filterByTime bool = true

	category := c.Query("category")
	if category == "" {
		filterByCategory = false
	}

	start, err := dateparse.ParseAny(c.Query("start"))
	end, err := dateparse.ParseAny(c.Query("end"))
	if err != nil {
		filterByTime = false
	}

	var data = CV.Data
	if filterByCategory {
		data = util.FilterByCategory(data, category)

	}

	if filterByTime {
		filteredData := util.FilterByTime(data, start, end)
		if err != nil {
			c.JSON(400, err)
		}
		data = filteredData //unsure
	}

	bbox, err := util.GetBBox(data)
	if err != nil {
		c.JSON(400, err)
	}
	sort.Sort(model.CVData(data))

	c.JSON(200, gin.H{
		"Person": CV.Person,
		"Data":   data,
		"BBox":   bbox,
	})

}


// @summary shows all Data but without a Geo-BoundingBox
// @ID get-CV
// @Accept  json
// @Produce  json
// @Success 200 {object} model.CV "ok"
// @Failure 400 {string} string "vermutl datei nicht gefunden"
// @Router /cv [get]
func GET_CV(c *gin.Context) {
	c.JSON(200, CV)
}


// @Description not yet implemented - need a CVEntry object in the body
// @ID post-cv/
// @summary not yet implemented - send a new Event of your life
// @Accept  json
// @Produce  json
// @Success 200 {string} string "ok"
// @Failure 400 {string} string "vermutl datei nicht gefunden"
// @Router /cv [post]
func POST_CVEntry(c *gin.Context) {
}


// @ID delete-cv/
// @summary not yet implemented - delete an element by name property the body
// @Accept  json
// @Produce  json
// @Success 200 {string} string "ok"
// @Failure 400 {string} string "vermutl datei nicht gefunden"
// @Router /cv [delete]
//TODO:implement
func DELETE_CVEntry(c *gin.Context) {
}
