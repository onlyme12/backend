package util

import (
	"../model"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/araddon/dateparse"
	"github.com/thoas/go-funk"
	"log"
	"os"
	"strconv"
	"time"
)

func LoadData(path string) model.CV {
	var myCV model.CV
	configFile, err := os.Open(path)
	if err != nil {
		log.Print("opening config file", err.Error())
	}

	jsonParser := json.NewDecoder(configFile)
	if err = jsonParser.Decode(&myCV); err != nil {
		log.Print("parsing config file", err.Error())
	}
	return myCV
}

//todo: rightHalfOutside & overall doesn'nt work
// Probleme mit funk?? ordentliches Date/Time objekt suchen bzw. serialisieren.
func FilterByTime(cvData []model.CVEntry, leftB time.Time, rightB time.Time) []model.CVEntry {

	data := funk.Filter(cvData, func(entry model.CVEntry) bool {
		entryStart, err := dateparse.ParseAny(entry.TimeRange.Start)
		entryEnd, err := dateparse.ParseAny(entry.TimeRange.End)
		if err != nil {
			var errEntry []model.CVEntry
			errEntry = append(errEntry, entry)
			fmt.Printf("data corrupted:")
			fmt.Printf("%+v", entry)
			return false
		}
		debugFilterTime(entry, leftB, entryStart, entryEnd, rightB)

		inner := entryEnd.After(leftB)
		if rightB.Before(entryStart) && rightB.Before(entryEnd) && leftB.Before(entryStart) && leftB.Before(entryEnd) {
			return false
		}
		//outer := leftB.Before(entryStart) && rightB.After(entryEnd)
		res := inner
		return res

	})
	return data.([]model.CVEntry)
}
func debugFilterTime(entry model.CVEntry, leftB time.Time, entryStart time.Time, entryEnd time.Time, rightB time.Time) {
	fmt.Println("--------------------------------------------------------")
	fmt.Println("Name: " + entry.Name)
	fmt.Println("leftBound: " + leftB.String())
	fmt.Println("Elem-leftB: " + entryStart.String())
	fmt.Println("Elem-rightB: " + entryEnd.String())
	fmt.Println("rightBound: " + rightB.String())
	fmt.Println("--------------------------------------------------------")
	fmt.Println("")
	fmt.Println("lb before entryStart: " + strconv.FormatBool(leftB.Before(entryStart)))
	fmt.Println("lb before entryEnd: " + strconv.FormatBool(leftB.Before(entryEnd)))
	fmt.Println("")
	fmt.Println("lb after entryStart: " + strconv.FormatBool(leftB.After(entryStart)))
	fmt.Println("lb after entryEnd: " + strconv.FormatBool(leftB.After(entryEnd)))
	fmt.Println("")
	fmt.Println("rb before entryStart: " + strconv.FormatBool(rightB.Before(entryStart)))
	fmt.Println("rb before entryEnd: " + strconv.FormatBool(rightB.Before(entryEnd)))
	fmt.Println("")
	fmt.Println("rb after entryStart: " + strconv.FormatBool(rightB.After(entryStart)))
	fmt.Println("rb after entryEnd: " + strconv.FormatBool(rightB.After(entryEnd)))
}

func FilterByCategory(cvData []model.CVEntry, category string) []model.CVEntry {
	data := funk.Filter(cvData, func(cvEntry model.CVEntry) bool {
		return cvEntry.Category == category
	})
	return data.([]model.CVEntry)
}

func GetBBox(filteredData []model.CVEntry) (model.BBox, error) {
	var bbox model.BBox
	bbox.Max.X, bbox.Max.Y = 0, 0
	bbox.Min.X, bbox.Min.Y = 1000000 , 1000000

	for _, cvEntry := range filteredData {

		for _, place := range cvEntry.Places {
			pLat, err := strconv.ParseFloat(place.Location.Lat, 64)
			pLon, err := strconv.ParseFloat(place.Location.Lon, 64)

			if err != nil {
				return bbox, errors.New("BBox: can't parse Float")
			}

			if pLat > bbox.Max.X {
				bbox.Max.X = pLat
			}
			if pLon > bbox.Max.Y {
				bbox.Max.Y = pLon
			}

			if pLat < bbox.Min.X {
				bbox.Min.X = pLat
			}
			if pLon < bbox.Min.Y {
				bbox.Min.Y = pLon
			}
		}
	}

	return bbox, nil
}
