package model

import (
	"github.com/araddon/dateparse"
)

type CVData []CVEntry

type CV struct {
	Person struct {
		Email string `json:"email"`
		Name  struct {
			First   string `json:"first"`
			Surname string `json:"surname"`
			Last    string `json:"last"`
		} `json:"name"`
		Birthday   string `json:"birthday"`
		PicPathRel string `json:"picPathRel"`
	} `json:"person"`
	Data CVData `json:"data"`
}

func (p CVData) Len() int {
	return len(p)
}

// Define compare
func (p CVData) Less(i, j int) bool {
	pi := dateparse.MustParse(p[i].TimeRange.End)
	pj := dateparse.MustParse(p[j].TimeRange.End)
	return pi.After(pj)
}

// Define swap over an array
func (p CVData) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

type CVEntry struct {
	Name      string    `json:"name"`
	TimeRange TimeRange `json:"timeRange"`
	Places    []Place   `json:"places"`
	Links     []string  `json:"links,omitempty"`
	Category  string    `json:"category"`
	Techstack string    `json:"techstack,omitempty"`
	Type      string    `json:"type,omitempty"`
}

type Place struct {
	Name     string   `json:"name"`
	Location Location `json:"location"`
}

type Location struct {
	Lat string `json:"lat"`
	Lon string `json:"lon"`
}
type TimeRange struct {
	Start string `json:"start"`
	End   string `json:"end"`
}
