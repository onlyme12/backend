package model

type BBox struct {
	Min struct {
		X float64
		Y float64
	}

	Max struct {
		X float64
		Y float64
	}
}
