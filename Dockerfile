FROM golang:alpine
RUN apk add --update --no-cache ca-certificates git # RUN  apk update && apk add git && go get gopkg.in/natefinch/lumberjack.v2
EXPOSE 8080
RUN mkdir /app
ADD . /app
WORKDIR /app
RUN go get -d -v
#CMD ["ls"]
RUN go build -o main .
RUN adduser -S -D -H -h /app appuser
USER appuser
COPY ./data/ffischer/cv.json ./data/ffischer/cv.json
CMD ["./main"]
